#!/usr/bin/env python3
import argparse
from copy import deepcopy
from math import pow
import os


import numpy as np
from obspy.core.inventory.util import Equipment, Site
from obspy.core.inventory.response import InstrumentSensitivity
from obspy.core.inventory.response import PolesZerosResponseStage
from obspy.core.inventory.response import ResponseStage
from obspy.core.inventory.response import CoefficientsTypeResponseStage
from obspy.core.inventory.response import FIRResponseStage
from obspy.core.inventory.response import Response
from obspy.core.inventory.channel import Channel
from obspy.core.inventory.station import Station
from obspy.core.inventory.network import Network
from obspy.core.inventory.inventory import Inventory
from obspy.io.rg16.core import (_read_initial_headers, _cmp_nbr_headers,
                                _make_trace)
import utm
import yaml


def extr_common_info_nodes(path_yaml):
    """
    Read the yaml file containing common informations about nodes.
    """
    with open(path_yaml) as conf_yaml:
        common_nodes_info = yaml.load(conf_yaml)
    return common_nodes_info


def convert_coordinates(easting, northing, hemisphere, zone):
    """
    Convert UTM coordinates (easting, northing) in WGS84 coordinates.
    Return: latitude, longitude.
    """
    if hemisphere == 'N':
        lat, lon = utm.to_latlon(easting, northing, zone, northern=True)
    else:
        lat, lon = utm.to_latlon(easting, northing, zone, northern=False)
    return lat, lon


def format_date(str_date):
    """
    Format the date
    """
    if ' ' in str_date:
        str_date = str_date.split()
        str_date = '{} {} {}'.format(str_date[0], str_date[1], str_date[2])
    return str_date


def struct_info(own_node_info, common_nodes_info, network):
    """
    Structure the informations relative to the node.
    """
    node_info = deepcopy(common_nodes_info)
    response_stage_2 = node_info['channel']['response']['response_stages'][1]
    response_stage_2['stage_gain'] = np.ceil(pow(10, own_node_info['preamp_gain'] / 20))
    node_info['channel']['latitude'] = own_node_info['latitude']
    node_info['channel']['longitude'] = own_node_info['longitude']
    node_info['channel']['depth'] = own_node_info['depth']
    node_info['channel']['location_code'] = own_node_info['location_code']
    node_info['channel']['code'] = own_node_info['code']
    node_info['channel']['sample_rate'] = own_node_info['sampling_rate']
    node_info['channel']['sample_rate_ratio_number_samples'] = own_node_info['sampling_rate']
    node_info['channel']['start_date'] = own_node_info['start_date']
    node_info['channel']['end_date'] = own_node_info['end_date']
    node_info['station']['code'] = own_node_info['station']
    node_info['station']['latitude'] = own_node_info['latitude']
    node_info['station']['longitude'] = own_node_info['longitude']
    node_info['station']['creation_date'] = own_node_info['start_date']
    node_info['station']['termination_date'] = own_node_info['end_date']
    node_info['station']['start_date'] = own_node_info['start_date']
    node_info['station']['end_date'] = own_node_info['end_date']
    node_info['network']['code'] = network
    return node_info


def cmp_normalization_factor(zeros, poles, fn):
    """
    Computes the normalization factor from the zeros, the poles and the
    normalization frequency.
    """
    for i in range(len(poles)):
        poles[i] = complex(poles[i])
    for i in range(len(zeros)):
        zeros[i] = complex(zeros[i])
    num = np.prod(2j*np.pi*fn-np.array(poles))
    denom = np.prod(2j*np.pi*fn-np.array(zeros))
    normalization_factor = np.absolute(num/denom)
    return normalization_factor


def break_symmetry(fir):
    if fir['symmetry'].lower() == 'odd':
        return np.append(np.array(fir['coefficients']),
                         np.array(fir['coefficients'][-2::-1]))
    elif fir['symmetry'].lower() == 'even':
        return np.append(np.array(fir['coefficients']),
                         np.array(fir['coefficients'][-1::-1]))
    elif fir['symmetry'].lower() == 'none':
        return np.array(fir['coefficients'])


def make_norm_coeff(fir):
    scale_factor = np.sum(break_symmetry(fir))
    return np.divide(fir['coefficients'], scale_factor, dtype=np.float128)


def calc_delay(fir, sps):
    return break_symmetry(fir).argmax() / sps


def make_response_stages(stages_info, phase_filter, sample_rate):
    """
    Return the response stages of the node.
    """
    list_resp_stages = []
    for ind, stage in enumerate(stages_info):
        stage_sequence_number = ind + 1
        if stage_sequence_number == 1 or stage_sequence_number == 2:
            pz_transfer_function_type = stage['pz_transfer_function_type']
            normalization_frequency = stage['normalization_frequency']
            fn = cmp_normalization_factor(stage['zeros'], stage['poles'],
                                          normalization_frequency)
            resp_stage = PolesZerosResponseStage(stage_sequence_number,
                                                 stage['stage_gain'],
                                                 stage['stage_gain_frequency'],
                                                 stage['input_units'],
                                                 stage['output_units'],
                                                 pz_transfer_function_type,
                                                 normalization_frequency,
                                                 stage['zeros'],
                                                 stage['poles'],
                                                 normalization_factor=fn)
        if stage_sequence_number == 3:
            current_sps = stage['decimation_input_sample_rate']
            resp_stage = CoefficientsTypeResponseStage(stage_sequence_number,
                                                       stage['stage_gain'],
                                                       0,
                                                       stage['input_units'],
                                                       stage['output_units'],
                                                       'DIGITAL',
                                                       numerator=[],
                                                       denominator=[],
                                                       decimation_input_sample_rate=current_sps,
                                                       decimation_factor=1,
                                                       decimation_offset=0,
                                                       decimation_delay=0,
                                                       decimation_correction=0)
        if stage_sequence_number == 4:
            n = stage['decimation_factor']
            poles = np.zeros(5*(n-1))
            zeros = np.array([])
            for k in np.arange(1, n):
                for y in range(5):
                    cos_sin = np.cos(2*k*np.pi/n) + 1j*np.sin(2*k*np.pi/n)
                    zeros = np.append(zeros, cos_sin)
            resp_stage = PolesZerosResponseStage(stage_sequence_number, 1, 0,
                                                 'COUNTS', 'COUNTS',
                                                 'DIGITAL (Z-TRANSFORM)',
                                                 0, zeros, poles,
                                                 normalization_factor=1/n**5,
                                                 decimation_input_sample_rate=current_sps,
                                                 decimation_factor=n,
                                                 decimation_offset=0,
                                                 decimation_delay=0,
                                                 decimation_correction=0)
            current_sps /= n
        if stage_sequence_number > 4:
            try:
                fir = stage[phase_filter]
            except KeyError:
                fir = stage
            coeff = make_norm_coeff(fir)
            delay = calc_delay(fir, current_sps)
            resp_stage = FIRResponseStage(stage_sequence_number,
                                          1, 0, 'COUNTS', 'COUNTS',
                                          symmetry=fir['symmetry'],
                                          coefficients=list(coeff),
                                          decimation_input_sample_rate=current_sps,
                                          decimation_factor=stage['decimation_factor'],
                                          decimation_offset=0,
                                          decimation_delay=delay,
                                          decimation_correction=delay)
            current_sps /= stage['decimation_factor']
        list_resp_stages.append(resp_stage)
    return list_resp_stages


def cmp_overall_sensitivity(response_stages):
    """
    Compute the overall sensitivity from the stages of the instrumental
    response.
    """
    overall_sensitivity = 1
    for stage in range(3):
        response_stage = response_stages[stage]
        gain = response_stage.stage_gain
        overall_sensitivity *= gain
    return overall_sensitivity


def make_instrument_sensitivity(overall_sensitivity, instru_info):
    """
    Return the instrument sensitivity of the node.
    """
    instru_sens = InstrumentSensitivity(overall_sensitivity,
                                        instru_info['frequency'],
                                        instru_info['input_units'],
                                        instru_info['output_units'])
    return instru_sens


def make_response(resp_info, phase_filter, sample_rate):
    """
    Return the response of the node.
    """
    response_stages = make_response_stages(resp_info['response_stages'],
                                           phase_filter, sample_rate)
    instru_info = resp_info['instrument_sensitivity']
    overall_sensitivity = cmp_overall_sensitivity(response_stages)
    instrument_sensitivity = make_instrument_sensitivity(overall_sensitivity,
                                                         instru_info)
    resp = Response(instrument_sensitivity=instrument_sensitivity,
                    response_stages=response_stages)
    return resp


def make_channels(chan, phase_filter):
    """
    Return the three channels for each node.
    """
    chans = []
    components = ['2', '3', '4']
    equi = Equipment(type=chan['equipment']['type'],
                     manufacturer=chan['equipment']['manufacturer'],
                     model=chan['equipment']['model'])
    resp = make_response(chan['response'], phase_filter, chan['sample_rate'])
    for component in components:
        srrnsa = chan['sample_rate_ratio_number_samples']
        srrnse = chan['sample_rate_ratio_number_seconds']
        clock_drift = chan['clock_drift_in_seconds_per_sample']
        chans.append(Channel(chan['code'] + component,
                             chan['location_code'],
                             chan['latitude'],
                             chan['longitude'],
                             chan['elevation'],
                             chan['depth'],
                             sample_rate=chan['sample_rate'],
                             sample_rate_ratio_number_samples=srrnsa,
                             sample_rate_ratio_number_seconds=srrnse,
                             storage_format=chan['storage_format'],
                             clock_drift_in_seconds_per_sample=clock_drift,
                             calibration_units=chan['calibration_units'],
                             equipment=equi,
                             response=resp,
                             start_date=chan['start_date'],
                             end_date=chan['end_date'],
                             restricted_status=chan['restricted_status']))
    return chans


def make_station(sta, site_name, chans):
    """
    Return the object station.
    """
    site = Site(site_name)
    station = [Station(sta['code'], sta['latitude'], sta['longitude'],
                       sta['elevation'], site=site, channels=chans,
                       start_date=sta['start_date'], end_date=sta['end_date'],
                       creation_date=sta['start_date'],
                       termination_date=sta['end_date'])]
    return station


def make_network(network, sta):
    """
    Return a Network object
    """
    network = [Network(network['code'], stations=sta,
                       restricted_status=network['restricted_status'])]
    return network


def make_inventory(node_info, site_name, phase_filter):
    """
    Create inventory from dictionnaries containing all the needed informations.
    """
    chans = make_channels(node_info['channel'], phase_filter)
    station = make_station(node_info['station'], site_name, chans)
    network = make_network(node_info['network'], station)
    inventory = Inventory(network, 'Strasbourg', sender='EOST')
    return inventory


def extract_info(path_node_fcnt, hemisphere, zone):
    """
    Extract 9 informations from fcnt file:
        - preamplification gain
        - depth of the node
        - latitude of the node
        - longitude of the node
        - location code
        - code channel
        - acquisition start time
        - acquisition stop time
        - sampling rate
        - station name
    Write them in a dictionnary
    """
    own_node_info = {}
    initial_headers = _read_initial_headers(path_node_fcnt)
    extended_header = initial_headers['extended_headers']['1']
    own_node_info['start_date'] = extended_header['start_time_ru']
    own_node_info['end_date'] = extended_header['pick_up_time']
    # extract one hour of data to be sure there is at least one trace in the
    # stream object.
    with open(path_node_fcnt, 'rb') as fi:
        header_count = _cmp_nbr_headers(fi)
        trace_block_start = 32 * (2 + sum(header_count))
        trace = _make_trace(fi, trace_block_start, True, False, True)
    trace_header = trace.stats.rg16['trace_headers']
    own_node_info['preamp_gain'] = trace_header['preamp_gain']
    easting = trace_header['receiver_point_final_x']
    northing = trace_header['receiver_point_final_y']
    if np.isclose(easting, 0) and np.isclose(northing, 0):
        easting = trace_header['receiver_point_pre_plan_x']
        northing = trace_header['receiver_point_pre_plan_y']
    lat, lon = convert_coordinates(easting, northing, hemisphere, zone)
    own_node_info['latitude'] = lat
    own_node_info['longitude'] = lon
    own_node_info['depth'] = trace_header['receiver_point_final_depth']
    own_node_info['sampling_rate'] = int(trace.stats.sampling_rate)
    own_node_info['station'] = trace.stats.station
    own_node_info['location_code'] = trace.stats.location
    own_node_info['code'] = trace.stats.channel[0:2]
    return own_node_info


def make_dataless(network, zone, hemisphere, phase_filter, site_name,
                  path_node_fcnt, rep_dataless):
    """
    Make dataless in STATIONXML format of the considered node.
    Fill an inventory object(ObsPy), thanks to the informations coming from
    fcnt file and from a configuration file (filed by the user).
    """
    own_node_info = extract_info(path_node_fcnt, hemisphere, zone)
    path_yaml_file = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'common_dataless.yaml')
    common_nodes_info = extr_common_info_nodes(path_yaml_file)
    node_info = struct_info(own_node_info, common_nodes_info, network)
    inventory = make_inventory(node_info, site_name, phase_filter)
    node_name = path_node_fcnt.split('/')[-1].split('.')[0]
    dataless_name = '{}.{}.xml'.format(network, node_name)
    path_dataless = os.path.join(rep_dataless, dataless_name)
    inventory.write(path_dataless, format="STATIONXML")


def make_job_list(network, zone, hemisphere, phase_filter, site_name,
                  storage_directory, rep_dataless):
    """
    Make job list
    """
    job_list = []
    for line_dir in os.listdir(storage_directory):
        path_line_dir = os.path.join(storage_directory, line_dir)
        fcnt_nodes_files = set([file.split('.')[0] for file
                                in os.listdir(path_line_dir)
                                if file.endswith('.fcnt')])
        for node in fcnt_nodes_files:
            node_fcnt = "{0}.0.0.fcnt".format(node)
            path_node_fcnt = os.path.join(path_line_dir, node_fcnt)
            job_list.append((network, zone, hemisphere, phase_filter,
                             site_name, path_node_fcnt, rep_dataless))
    return job_list


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create dataless in \
                                     STATIONXML format for each node')
    parser.add_argument('network', help='network name (expected 2 chars)')
    parser.add_argument('zone', help='UTM zone', type=int)
    parser.add_argument('hemisphere', help='hemisphere where the nodes\
                         were deployed (N or S)')
    parser.add_argument('site_name', help='name of the experience site')
    parser.add_argument('phase_filter', help='phase of the anti-alias filter,\
                        2 choices: minimum or linear.')
    parser.add_argument('storage_directory', help='path directory containing \
                        the directories of the receiver lines. fcnt files \
                        are present in each directory of receiver line.')
    parser.add_argument('rep_output', help='path directory where the \
                        dataless will be stored')
    args = parser.parse_args()
    network = args.network
    zone = args.zone
    hemisphere = args.hemisphere
    phase_filter = args.phase_filter
    site_name = args.site_name
    storage_directory = args.storage_directory
    rep_dataless = args.rep_output
    if phase_filter not in ['minimum', 'linear']:
        print('the argument phase filter has to be minimum or linear')
        raise ValueError
    job_list = make_job_list(network, zone, hemisphere, phase_filter,
                             site_name, storage_directory, rep_dataless)
    for job in job_list:
        make_dataless(*job)

